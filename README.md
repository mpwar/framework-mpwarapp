# Instrucciones para instalar el Framework

Para instalar el Framework se debe añadir el repositorio de éste en el fichero ```composer.json```:

```
  "repositories": [
    {
      "url": "https://github.com/vicaba/Mpwarfrk.git",
      "type": "git"
    }
  ]
```

Y añadir la dependencia al Framework:

```
  "require": {
    "php": "^5.6 || ^7.0",
    "vicaba/Mpwarfrk": "dev-master"
  }
```

Una configuración ejemplo de composer sería:

```
{
  "repositories": [
    {
      "url": "https://github.com/vicaba/Mpwarfrk.git",
      "type": "git"
    }
  ],
  "autoload": {
    "psr-4": {
      "App\\": "src/App"
    }
  },
  "require": {
    "php": "^5.6 || ^7.0",
    "vicaba/Mpwarfrk": "dev-master"
  },
  "minimum-stability": "dev"
}
```

Después ejecutar ```composer install```

# Entrega

En la carpeta ```app``` se encuentran:
* ```db.sql```: Dump de la base de datos
* ```diDev.yml```: Básicamente es una copia de ```di.yml```, simplemente cambia la carpeta de templates.
* ```VirtualHosts``` dump de los VirtualHosts creados. mpwarrapp.dev apunta a index.php y mpwarapp.test apunta a indexDev.php.

En el fichero de rutas se puede observar como algunas de ellas reciben parámetros por la URL y/o devuelven un JSON.
