<?php

session_start();

date_default_timezone_set("Europe/London");
ini_set('display_errors', 'On');
error_reporting(E_ALL);

require("../vendor/autoload.php");

$yamlParser = new \Mpwarfrk\Component\Parser\YamlParser();
$di = \Mpwarfrk\Component\Di\Di::fromFile(__DIR__."/../app/diDev.yml", $yamlParser);

$router = \Mpwarfrk\Component\Router\Router::fromFile(__DIR__."/../app/routes.yml", $yamlParser);

$di->set("router", $router);

$bootstrap = new \Mpwarfrk\Component\Bootstrap\Bootstrap($di);

$request = new \Mpwarfrk\Component\Http\Request\Request($_SERVER, $_GET, $_POST, getallheaders(), $_SESSION, $_COOKIE);

$response =  $bootstrap->run($request);

$response->send();

echo (new \Mpwarfrk\Component\Profiler\ProfilerAggregator($di->getProfilingServices()))->getProfilingInformationAsString();