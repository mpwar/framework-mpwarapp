<?php

namespace App\User\Controller;


use Mpwarfrk\Component\Http\Request\Request;

class SignUp extends \Mpwarfrk\Component\Controller\BaseController
{
    public function getForm(Request $request)
    {
        return $this->templateEngine->render("user_signup");
    }

    public function submit(Request $request) {
        $name = $request->getRequest("name");
        $password = $request->getRequest("password");

        $this->db->execute("INSERT INTO user (name, password) VALUES (\"$name\", \"$password\")");

        return "$name with $password has been signed up";
    }
}