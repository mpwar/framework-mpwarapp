<?php

namespace App\Index\Controller;

use Mpwarfrk\Component\Controller\BaseController;
use Mpwarfrk\Component\Http\Request\Request;

class Index extends BaseController
{

    public function index(Request $request) {
        $body = $this->templateEngine->render("index", ["name" => "victor"]);
        return $body;
    }

    public function urlParams(Request $request) {
        $name = $request->getUrlParam("name");
        $body = $this->templateEngine->render("index", ["name" => $name]);
        return $body;
    }
}