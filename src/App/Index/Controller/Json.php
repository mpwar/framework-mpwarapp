<?php

namespace App\Index\Controller;


use Mpwarfrk\Component\Controller\BaseController;
use Mpwarfrk\Component\Http\Request\Request;
use Mpwarfrk\Component\Http\Response\JsonResponse;

class Json extends BaseController
{

    public function jsonParams(Request $request) {
        $name = $request->getUrlParam("name");
        $body = json_encode(["name" => $name]);
        return new JsonResponse($body, 201);
    }
}