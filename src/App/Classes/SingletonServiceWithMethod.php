<?php

namespace App\Classes;


class SingletonServiceWithMethod
{

    public $someVar;

    private static $instance = null;

    public static function getInstance()
    {
        if (self::$instance == null) {
            self::$instance = new static;
            self::$instance->someVar = "0";
        }
        return self::$instance;
    }
}