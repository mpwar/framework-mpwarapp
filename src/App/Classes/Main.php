<?php

namespace App\Classes;


class Main
{

    public $service;

    public $singletonService;

    public $singletonServiceWithMethod;


    public function __construct(Service $service, SingletonService $singletonService, SingletonServiceWithMethod $singletonServiceWithMethod)
    {
        $this->service = $service;
        $this->singletonService = $singletonService;
        $this->singletonServiceWithMethod = $singletonServiceWithMethod;
    }
}